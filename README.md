## trinket-user 10 QKQ1.200209.002 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: r5x
- Brand: Realme
- Flavor: ancient_r5x-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.rmnare.20210928.044612
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Realme/ancient_r5x/r5x:11/RQ3A.210905.001/rmnareshkumar32109280443:userdebug/release-keys
- OTA version: 
- Branch: trinket-user-10-QKQ1.200209.002-release-keys
- Repo: realme_r5x_dump_11515


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
